import React from 'react';

const Item = (props) => {
    return (<li className={props.status}>
            {props.message}
            <div className={'buttons'}>
                <div onClick={props.handleClick} className={props.status + ' status'}>{props.status}</div>
                <div className={'remove'} onClick={props.removeItem}>x</div>
            </div>
    </li>)
};

export default Item;