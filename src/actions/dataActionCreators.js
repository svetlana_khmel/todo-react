import * as types from '../constants/actionTypes';

export function getData (data) {
    return {
        type: types.GET_DATA,
        payload: data
    }
}

export const toggleStatus =  (id) => {
    return {
        type: types.TOGGLE_STATUS,
        id
    }
};

export const addItem =  (item) => {
    return {
        type: types.ADD_ITEM,
        payload: item
    }
};

export const removeItem = (id) => {
    return {
        type: types.REMOVE_ITEM,
        id
    }
};

export const filterArray =  (item) => {
    return {
        type: types.FILTER_ARRAY,
        payload: item
    }
};

export const loadData = () => {
    return (dispatch, getState) => {
        return fetch('/json', {
            method: 'GET',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            return response.json();
        })
        .then((data) => {
            dispatch(getData(data));
        })
    }
};