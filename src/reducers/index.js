import { combineReducers } from 'redux';
import data from './dataReducer';
import filter from './filterReducer';


const rootReducer = combineReducers({
    data,
    filter
});

export default rootReducer;