import * as types from '../constants/actionTypes';

let initialState  = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case types.GET_DATA:
            return [...action.payload, ...state];

        case types.TOGGLE_STATUS:
            // return state.map(todo =>
            //     (todo.id === action.id)
            //         ? {...todo, status: (todo.status === 'undone' ? todo.status = 'done' : todo.status = 'undone')}
            //         : todo
            // );

            return state.map(todo => {
                if (todo.id === action.id) {
                    return Object.assign({}, todo, {
                        status: todo.status === 'undone' ? 'done' : 'undone'
                    })
                }
            });


        case types.ADD_ITEM:

            return [...state, action.payload];

        case types.REMOVE_ITEM:

           return state.filter((item) => {

                if (item.id !== action.id) {
                    return item;
                }
            });

        default:
            return state;
    }
}