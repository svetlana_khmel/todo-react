import reducer from '../dataReducer';
import * as types from '../../constants/actionTypes';

describe('data reducer', () => {

    let initialState = [];

    it('should return the initial state', () => {
        expect(reducer(undefined, [])).toEqual(initialState)
    });

    it('should handle ADD_ITEM', () => {

        expect(
            reducer([], {
                type: types.ADD_ITEM,
                payload: {
                    "id":"4",
                    "status": "undone",
                    "text": "Some plans here 4"
                }
            })
        ).toEqual([{
                "id":"4",
                "status": "undone",
                "text": "Some plans here 4"
            }]);

        expect(
            reducer(
                [
                    {
                        "id":"0",
                        "status": "undone",
                        "text": "Some plans here"
                    }
                ],
                {
                    type: 'ADD_ITEM',
                    payload: {
                        "id":"1",
                        "status": "undone",
                        "text": "Some plans here"
                    }
                }
            )
        ).toEqual([
            {
                "id":"0",
                "status": "undone",
                "text": "Some plans here"
            },
            {
                "id":"1",
                "status": "undone",
                "text": "Some plans here"
            }
        ])
    })
});