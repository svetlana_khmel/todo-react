import { FILTER_ARRAY } from '../constants/actionTypes';

export default function (state = {'status': 'all'}, action) {
    switch (action.type) {
        case FILTER_ARRAY:
            return {
                ...state,
                status: action.payload.status
            };

        default:
            return state;
    }
}