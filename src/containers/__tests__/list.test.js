import React from 'react'
import { mount } from 'enzyme'
import List from '../List';

import Adapter from 'enzyme-adapter-react-15';

configure({ adapter: new Adapter() });

function setup() {
    const props = {
        //addTodo: jest.fn()
        loadData: jest.fn(),
        toggleStatus: jest.fn(),
        removeItem: jest.fn()
    };

    const enzymeWrapper = mount(<List {...props} />);

    return {
        props,
        enzymeWrapper
    }
}

describe('components', () => {
    describe('List', () => {
        it('should render self and subcomponents', () => {
            const { enzymeWrapper } = setup();

            expect(enzymeWrapper.find('div').hasClass('list')).toBe(true);

            expect(enzymeWrapper.find('h1').text()).toBe('todos');

            const todoInputProps = enzymeWrapper.find('TodoTextInput').props();
            expect(todoInputProps.newTodo).toBe(true);
            expect(todoInputProps.placeholder).toEqual('What needs to be done?')
        })

        // it('should call addTodo if length of text is greater than 0', () => {
        //     const { enzymeWrapper, props } = setup();
        //     const input = enzymeWrapper.find('TodoTextInput')
        //     input.props().onSave('')
        //     expect(props.addTodo.mock.calls.length).toBe(0)
        //     input.props().onSave('Use Redux')
        //     expect(props.addTodo.mock.calls.length).toBe(1)
        // })
    })
})