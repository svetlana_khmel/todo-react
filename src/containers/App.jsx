import React, { Component } from 'react';
import * as actions from '../actions/dataActionCreators';
import { connect } from "react-redux";

import {bindActionCreators} from 'redux';

import List from './List';
import Form from '../components/toDoForm';
import Filters from './Filters';

class App extends Component {
    constructor (props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.addItem = this.addItem.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.data !== this.props.data;
    }

    addItem () {
        const item = {};

        item.id = (this.props.data.length + 1).toString();
        item.status = 'undone';
        item.text = this.state.formValue;

        this.props.addItem(item);
    }

    onSubmit (e) {
        e.preventDefault();
        this.addItem();
    }

    handleInput (e) {
        this.setState({
            formValue : e.target.value
        });
    }

    render () {
        return (
            <div className='todo-list'>
                <h1>Todo list</h1>
                <Form onSubmit={this.onSubmit} handleInput={(e) => this.handleInput(e)} />
                <List />
                <Filters />
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        data: state.data
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({addItem: actions.addItem}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(App);