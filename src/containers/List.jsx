import React, { Component } from 'react';
import * as actions  from '../actions/dataActionCreators';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';

import Item from '../components/toDoItem';

class List extends Component {
    constructor (props) {
        super(props);

        this.receiveData = this.receiveData.bind(this);
        this.filterData = this.filterData.bind(this);
    }

    componentDidMount () {
        this.receiveData();
    }

    receiveData () {
        this.props.loadData();
    }

    setStatus(e, id) {
        e.stopPropagation();

        this.props.toggleStatus(id);
    }

    removeItem (id) {
        this.props.removeItem(id);
    }

    filterData () {
        let status = this.props.filter.status;

        if (!this.props.data) return;

        let filteredArray = this.props.data.filter(function (item) {
            if (status === 'all') {
                return item;
            }

            if (status === 'undone') {
                if (item.status == status) {
                    return item;
                }
            }

            if (status === 'done') {
                if (item.status === status) {
                    return item;
                }
            }
        });

        return filteredArray;
    }

    render () {
        let items = this.filterData();

        return (
            <div className="list">
                {items ? (
                    <ul>
                        {items.map((item, index) => <Item key={index} message={item.text} status={item.status} handleClick={(e) => this.setStatus(e, item.id)} removeItem={(e) => this.removeItem(item.id)}/> )}
                    </ul>
                ) : (
                    <div>Loading...</div>
                )}
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        data: state.data,
        filter: state.filter,
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({loadData: actions.loadData, toggleStatus: actions.toggleStatus, removeItem: actions.removeItem}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(List);

